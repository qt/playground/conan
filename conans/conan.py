import os
import sys
import pexpect
from contextlib import closing

from conans.client.command import main


class LocalConanServerError(Exception):
    pass


def requires_local_conan_server(args):
    if "qt-conan-local-server" in args:
        print("Remote 'qt-conan-local-server' defined .. launching local conan_server")
        return True
    return False


def launch_local_conan_server():
    if getattr(sys, 'frozen', False):
        # If the application is run as a bundle, the PyInstaller bootloader
        # extends the sys module by a flag frozen=True and sets the app
        # path into variable sys._MEIPASS

        # But this setup is one-file executable so use the following
        ext = ".exe" if os.name == 'nt' else ""
        conan_server = os.path.join(os.path.dirname(sys.executable), 'conan_server' + ext)
    else:
        conan_server = os.environ.get('CONAN_SERVER_EXECUTABLE')

    if not os.path.exists(conan_server) or not os.access(conan_server, os.X_OK):
        raise Exception("Qt conan client: Unable to launch local conan server: {0}".format(conan_server))

    # pexpect works better vs. subprocess and PIPEs to capture output if the program asks for interactive input,
    # like conan_server does ('Hit ctrl-c to stop')
    print("Qt conan client: Launching local conan server: ", conan_server)
    child = pexpect.spawn(conan_server)
    lineCheckLimit = 20
    # wait and check that the server is up and running
    for line in child:
        _line = line.decode("utf-8").strip()
        print(_line)
        if _line.startswith("Listening on"):
            # server is running
            break
        lineCheckLimit -= 1
        if not lineCheckLimit:
            child.close()
            msg = "Unable to start local Conan server: {0}".format(conan_server)
            print(msg)
            raise LocalConanServerError(msg)
    return child


def run():
    if requires_local_conan_server(sys.argv[1:]):
        with closing(launch_local_conan_server()):
            main(sys.argv[1:])
    else:
        main(sys.argv[1:])


if __name__ == '__main__':
    run()
